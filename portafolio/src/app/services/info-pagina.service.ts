import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { InfoPaginaInterface } from '../interfaces/info-pagina-interface';

@Injectable({
  providedIn: 'root'
})
export class InfoPaginaService {
info:InfoPaginaInterface={};
cargada:boolean=false;
  constructor(private http:HttpClient) {

    this.http.get('assets/data/data-pagina.json')
    .subscribe((response:InfoPaginaInterface) =>{
      console.log(response);
      console.log(response['email']);
      this.cargada=true;
      this.info=response;
      console.log(this.info);
    });
   }
}
